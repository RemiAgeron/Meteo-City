function cl(_) { return console.log(_) }

navigator.geolocation.getCurrentPosition(function(position) {
  fetch('http://api.openweathermap.org/geo/1.0/reverse?lat=' + position.coords.latitude + '&lon=' + position.coords.longitude + '&limit=5&appid=7fe6cd6de57bded37894e69a5d6035eb')
  .then(response => response.json())
  .then(function(positionData) {
    getInfo(positionData[0].name)
  })
})

let input = document.getElementById('input')
let citylist = document.getElementById('citylist')
let cityName = document.getElementById('cityName')
let output = document.getElementById('output')
let refresh = document.getElementById('refresh')
let loading = document.getElementById('loading')
let timeout = 600
let icon = document.getElementById('icon')
let temp = document.getElementById('temp')
let wind = document.getElementById('wind')
let dir = document.getElementById('dir')
let feel = document.getElementById('feel')
let max = document.getElementById('max')
let min = document.getElementById('min')
let humi = document.getElementById('humi')


function getInfo(name) {
  input.blur()
  loading.style.display = "initial"
  fetch('http://api.openweathermap.org/data/2.5/weather?q=' + name + '&units=metric&lang=fr&appid=7fe6cd6de57bded37894e69a5d6035eb')
  .then(response => response.json())
  .then(function(weatherData) {
    setTimeout(() => {
      loading.style.display = "none"
      refresh.style.display = "initial"
      cityName.style.display = "initial"
      if (weatherData.cod === "404") {
        cityName.textContent = 'Ville inconnue'
      } else {
        output.style.display = "initial"
        cityName.textContent = weatherData.name
        icon.src = 'http://openweathermap.org/img/wn/' + weatherData.weather[0].icon + '@2x.png'
        icon.title = weatherData.weather[0].description
        temp.textContent = String(Math.round(weatherData.main.temp))
        wind.textContent = String(Math.round((weatherData.wind.speed * 3.6) * 10) / 10)
        dir.style.rotate = weatherData.wind.deg + "deg"
        feel.textContent = String(Math.round(weatherData.main.feels_like))
        max.textContent = String(Math.round(weatherData.main.temp_max))
        min.textContent = String(Math.round(weatherData.main.temp_min))
        humi.textContent = String(weatherData.main.humidity)
      }
    }, timeout)
  })
}

input.addEventListener("input", function() {
  citylist.textContent = null
  if (input.value.length >= 3) {
    fetch('https://geo.api.gouv.fr/communes?nom=' + input.value + '&fields=code,nom,departement,population')
    .then(response => response.json())
    .then( function(listOfCity) {
      listOfCity = listOfCity.sort((a, b) => b.population - a.population)
      for (let i = 0; i < 6; i++) {
        let option = document.createElement("option")
        option.value = listOfCity[i].nom
        option.textContent = listOfCity[i].nom + ', ' + listOfCity[i].departement.nom
        citylist.appendChild(option)
        if (i == listOfCity.length - 1) { break }
      }
    })
  }
})

input.addEventListener("keyup", function(event) { 
  cityName.style.display = "none"
  output.style.display = "none"
  if (event.key === "Enter") { getInfo(input.value) }
})

refresh.addEventListener("click", function() {
  cityName.style.display = "none"
  output.style.display = "none"
  getInfo(input.value)
})
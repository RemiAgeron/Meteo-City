<h3 align="center">Meteo City</h3>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Tableau des Matières</summary>
  <ol>
    <li>
      <a href="#about-the-project">À propos du projet</a>
      <ul>
        <li><a href="#built-with">Technologies</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Bien commencer</a>
      <ul>
        <li><a href="#prerequisites">Pré-requis</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Exemples d'utilisation</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## À propos du projet

Le projet est de proposer aux utilisateurs une interface simple, pour connaître la météo en choisissant entre :
  - partager sa localisation et obtenir des renseignements sur la météo actuelle de sa localisation.
  - voir la météo de n'importe quelle ville, les villes françaises étant admissibles à l'autocomplétion.

<p align="right">(<a href="#top">retour au début</a>)</p>



### Technologies

* [Html]
* [JavaScript]
* [Bootstrap]

<p align="right">(<a href="#top">retour au début</a>)</p>



<!-- GETTING STARTED -->
## Bien commencer

Avant toutes choses assuré vous d'avoir tous les pré-requis ci-dessous.

### Pré-requis

Vous devez impérativement avoir un ordinateur et une connection internet.

Vous pouvez également vérifier l'intégrité de votre ordinateur en consultant le site : https://www.ismycomputeronfire.com/
Lorsque vous êtes sûr que votre ordinateur n'est pas en feu, vous pouvez continuer.

<p align="right">(<a href="#top">retour au début</a>)</p>



<!-- USAGE EXAMPLES -->
## Exemples d'utilisation

Vous pouvez essayer d'aller sur le site

Si vous accepter de partager votre localisation, je vous promet de ni utiliser, ni vendre vos données personnelles, car premièrement ce n'est pas le but du projet et puis de toutes façons je ne sais pas comment faire.

Vous trouverez grâce à cette application :
 - une icône représentant la météo
 - la température
 - la vitesse du vent
 - et sa direction par rapport au Nord
 - la température ressentie
 - la valeur maximum
 - la valeur minimum de la journée
 - le pourcentage d'humidité

Si vous avez accepté de partager votre localisation, déjà merci pour la confiance que vous nous accordez, vous trouverez les données météorologiques de votre position.

Si vous souhaitez connaître la météo d'une ville en particulier, entrez-là dans la barre de recherche si elle se trouve dans les suggestions vous pouvez directement cliquez dessus sinon entrez le nom et appuyez sur la touche Entrer.

Si vous voulez savoir si la météo a évolué vous pouvez appuyez sur le bouton à droite de la barre de recherche pour rafraîchir les données sans changer la ville.

<p align="right">(<a href="#top">retour au début</a>)</p>
